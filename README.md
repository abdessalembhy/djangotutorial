Quand utiliser include()

Il faut toujours utiliser include() lorsque l’on veut inclure d’autres motifs d’URL. admin.site.urls est la seule exception à cette règle.